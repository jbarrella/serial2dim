// -*- mode: c++; c-basic-offset: 4; indent-tabs-mode: nil -*-

#ifndef SERPORT_HH
#define SERPORT_HH

#include <termios.h>
#include <cstdarg>

#include <string>


// ====================================================================
//
//  Class serport
//
//  Encapsulates low-level access to the serial port. It procides
//  functions to open and close the device and to send and receive
//  data. All IO is non-blocking, polling timeouts are appropriate
//  for a MGC647C gas controller.
//
// ====================================================================

class serport
{

public:

    // constructor
    serport(std::string dev="/dev/ttyS0", int brate = B9600);


    // Open and close the device and set/restore the terminal 
    // settings
    void open_dev();
    void close_dev();
    
    bool is_open() { return (fd != -1); }


    // Send the specified string (possibly given printf-style),
    // but do not wait for a response.
    void send(std::string command);
    void send(const char* command, ...);

    // Receive a string and return it as a STL string. The function 
    // returns when no data has been received for 10ms.
    std::string receive();
    
    // Send a command to the serial port, wait for 50ms for data
    // to occur on the serial line and check if data was received.
    // If no data was received, continue to wait and receive.
    std::string exec(std::string command);
    std::string exec(const char* command, ...);

protected:

    bool check_complete(char* buf, int len);
    
    std::string device;  // serial port device name
    int baudrate;        
    int fd;              // file descriptor

    struct termios oldtio;
    struct termios newtio;
};




#endif
