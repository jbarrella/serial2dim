

#include <iostream>
#include <iomanip>
#include <string>
#include <list>
#include <algorithm>

#include <cstring>

#include "commands.hh"
#include "serport.hh"
#include "services.hh"
#include "util.hh"

#include <dim/dis.hxx>

using namespace std;

void call_update  (service* svc) { svc->update(1,true); }
void call_process (command* cmd) { cmd->process(); }


int main(int argc, char** argv)
{

  cout << "Opening serial port" << endl;
  serport ser("/dev/ttyUSB0");//, B9660);
  ser.send("\r\n");
  usleep(50000);

  //services.push_back(service(&ser, "id",      "#\r\n"));

  //services.push_back(service(&ser, "status1", "S1\r\n", ""));
  //services.push_back(service(&ser, "Vmeas1",  "U1\r\n", ""));
  //services.push_back(service(&ser, "Imeas1",  "I1\r\n", ""));
  //services.push_back(service(&ser, "Vlimit1", "M1\r\n", ""));
  //services.push_back(service(&ser, "Ilimit1", "N1\r\n", ""));
  //services.push_back(service(&ser, "Vset1",   "D1\r\n", ""));
  //services.push_back(service(&ser, "Vramp1",  "V1\r\n", ""));


  // ====================================================================
  // Configuration of services
  // ====================================================================
  // note on intervals:
  //   x<<10 gives the time in milliseconds (approx)
  //   x<<20 gives the time in seconds (approx)


  cout << "Creating services" << endl;
  list<service*> services;
  list<command*> commands;

  // --------------------------------------------------------------------
  // drift voltage
  //
  service_nhqfloat* svc_VmeasD =
    new service_nhqfloat(&ser, "HV.00_2_0.DRIFT.VMeas", "U2", 200<<10);
  
  service_nhqfloat* svc_ImeasD =
    new service_nhqfloat(&ser, "HV.00_2_0.DRIFT.IMeas", "I2", 200<10);
  
  service_nhqfloat* svc_VsetD =
    new service_nhqfloat(&ser, "HV.00_2_0.DRIFT.VSet", "D2", 3<<20);
  
  //service_nhqfloat* svc_VrampD =
  //  new service_nhqfloat(&ser, "HV.00_2_0.DRIFT.Vramp", "V1", 20<<20);
  
  service_nhqfloat* svc_ItripD =
    new service_nhqfloat(&ser, "HV.00_2_0.DRIFT.ISet", "L2", 3<<20);
  
  //service_nhqfloat* svc_Vset1(&ser, "Vset1",  "D1");
  
  services.push_back(svc_VmeasD);
  services.push_back(svc_ImeasD);
  services.push_back(svc_VsetD);
  services.push_back(svc_ItripD);

  // --------------------------------------------------------------------
  // anode voltage
  //
  service_nhqfloat* svc_VmeasA =
    new service_nhqfloat(&ser, "HV.00_2_0.ANODE.VMeas", "U1", 200<<10);
  
  service_nhqfloat* svc_ImeasA =
    new service_nhqfloat(&ser, "HV.00_2_0.ANODE.IMeas", "I1", 200<10);
  
  service_nhqfloat* svc_VsetA =
    new service_nhqfloat(&ser, "HV.00_2_0.ANODE.VSet", "D1", 3<<20);
  
  service_nhqfloat* svc_ItripA =
    new service_nhqfloat(&ser, "HV.00_2_0.ANODE.ISet", "L1", 3<<20);

  services.push_back(svc_VmeasA);
  services.push_back(svc_ImeasA);
  services.push_back(svc_VsetA);
  services.push_back(svc_ItripA);

  
  //command_nhqfloat* cmd_ItripD =
  //  new command_nhqfloat("HV.00_2_0.DRIFT.Iset/CMD",svc_ItripD);


  // --------------------------------------------------------------------

  commands.push_back(new command_raw("HV_RAW_CMD", &ser));

  commands.push_back(new command_nhqfloat("HV.00_2_0.DRIFT.VSet/CMD",
					  svc_VsetD));
  
  commands.push_back(new command_nhqfloat("HV.00_2_0.DRIFT.ISet/CMD",
					  svc_ItripD));
  
  commands.push_back(new command_nhqfloat("HV.00_2_0.ANODE.VSet/CMD",
					  svc_VsetA));
  
  commands.push_back(new command_nhqfloat("HV.00_2_0.ANODE.ISet/CMD",
					  svc_ItripA));
  
  //services.push_back(service(&ser, "MStat1",  "T1\r\n", ""));
  
  //services.push_back(service(&ser, "status2", "S2\r\n", ""));
  //services.push_back(service(&ser, "Vmeas2",  "U2\r\n", ""));
  //services.push_back(service(&ser, "Imeas2",  "I2\r\n", ""));
  //services.push_back(service(&ser, "Vlimit2", "M2\r\n", ""));
  //services.push_back(service(&ser, "Ilimit2", "N2\r\n", ""));
  //services.push_back(service(&ser, "Vset2",   "D2\r\n", ""));
  //services.push_back(service(&ser, "Vramp2",  "V2\r\n", ""));
  //services.push_back(service(&ser, "Itrip2",  "L2\r\n", ""));
  //services.push_back(service(&ser, "MStat2",  "T2\r\n", ""));


  DimServer::start("HV");
  
  
  //svc_Itrip1.set(20e-7);

  cout << "start" << endl;

  while (1) {

    
    
    // call process() for all commands
    for_each(commands.begin(), commands.end(), call_process);

    // call update() for  all services
    for_each(services.begin(), services.end(), call_update);

    usleep(10000);

    
  }
  
  //cout << txt2hex(ser.exec("V1=5\r\n")) << endl;
  ////cout << txt2hex(ser.exec("L1=0020\r\n")) << endl;
  //cout << txt2hex(ser.exec("D1=0080\r\n")) << endl;
  //cout << txt2hex(ser.exec("G1\r\n")) << endl;
  
}

