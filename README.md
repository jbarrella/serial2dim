
DIM Servers for Devices connected to Serial Ports
=================================================

This repository contains DIM servers that communicate with hardware via a serial port. 
The first such DIM server controls the an Iseg NHQ high-voltage power supply used for 
the TRD chamber at UCT. 

The code might be useful for other project or other sites as well. It should also be a 
good starting point for other DIM servers that communicate via a serial port.

It is the first time that I am using CMake to build a project.