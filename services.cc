#include "services.hh"

#include "util.hh"

#include <iostream>
#include <iomanip>
#include <sstream>
#include <cstdio>
#include <cstdlib>
#include <cmath>

#include <Poco/RegularExpression.h>
#include <Poco/DateTimeFormatter.h>
#include <dim/dis.hxx>


using namespace std;

using Poco::DateTimeFormatter;

service::service(serport* device, string svcname, string cmd, int intvl)
  : dev(device), thename(svcname), command(cmd), interval(intvl)
{}

// -----------------------------------------------------------------------
void service::update(int maxretries, bool checkinterval)
{
  
  //cout << DateTimeFormatter::format(lastupdate, "%e %b %Y %H:%M") << endl;

  //if (checkinterval) {
  //  cout << DateTimeFormatter::format(lastupdate, "%e %b %Y %H:%M") << endl;
  //  //cout << interval << endl;
  //}
  
  if (checkinterval && ! lastupdate.isElapsed(interval) )
    return;
  
  bool fd_open = dev->is_open();
  if (!fd_open) dev->open_dev();

  for (int retry=0; retry<maxretries; retry++) {
    //dev->receive();
    dev->send(command);

    switch (receive_and_parse()) {

    case good:
      if (!fd_open) dev->close_dev();
      return;
      
    case wait:
      cerr << "error: receive_and_parse MUST handle waits" << endl;
      return;
      
    case fail:
      cout << "retry" << endl;
      usleep(100000);
      dev->receive();
      break;
    }
  }

  cerr << "failed to update service" << endl;
  if (!fd_open) dev->close_dev();

}

// -----------------------------------------------------------------------
service::parse_status_t service::receive_and_parse()
{
  string result = "";
  int maxread = 5;

  for (int i=0; i<maxread; i++) {

    result += dev->receive();

    if ( result.find("????") != string::npos ||
	 result.find("?WCN") != string::npos ||
	 result.find("?TOT") != string::npos ) {

      cerr << "received error code: " << result.substr(result.find("?"),4)
	   << endl;
      
      return fail;
    }
   
    
    switch ( parse(result) ) {
    case good:
      lastupdate.update();
      return good;

    case fail:
      cerr << "invalid response: " << result << endl;
      return fail;

    case wait:
      // continue in loop
      break;
    }
    
  }

  return fail;
}


// -----------------------------------------------------------------------
service::parse_status_t service::parse(std::string txt)
{
  //cout << "parsing: '" << txt << "'" << endl;
  
  if (txt.size() < 5)
    return wait;

  int count = 0;
  for (int i=0; i<txt.size(); i++) {
    if (txt[i]=='\n') count++;
  }
  
  if ( count >= 4 ) {
    //lastvalstr = thename + ": " + txt.substr(4, txt.size()-6);
    lastvalstr = txt.substr(4, txt.size()-6);
    return good;
  }

  // not done yet
  return wait;
}


// -----------------------------------------------------------------------
service_nhqfloat::service_nhqfloat(serport* device,
				   string svcname,
				   string c,
				   int intvl)
  : service(device, svcname, c+"\r\n"), cmd(c),
    dimsvc(NULL)
{
  dimsvc = new DimService(svcname.c_str(),val);
}


// -----------------------------------------------------------------------
service::parse_status_t service_nhqfloat::parse(std::string txt)
{
  //cout << "parsing: '" << txt2hex(txt) << "'" << endl;
  
  if (txt.size() < 9)
    return wait;

  Poco::RegularExpression re("[-+]?[0-9]{5}[-+][0-9]{2}");
  Poco::RegularExpression::Match match;

  if (re.match(txt,match)) {

    if (0) {
      cout << "found match at offset " << match.offset
	   << " length " << match.length << ": "
	   << "'" << txt.substr(match.offset,match.length-3) << "'"
	   << "'" << txt.substr(match.offset+match.length-3,3) << "'"
	   << endl;
    }

    // the mantissa are the first n-3 chars
    valm=atof(txt.substr(match.offset,match.length-3).c_str());

    // the exponent are the last 3 chars
    vale=atof(txt.substr(match.offset+match.length-3,3).c_str());
    
    if ( fabs( valm*pow(10.,vale) - val ) > 0.001*val ) {
      val = valm*pow(10.,vale);
      cout << "value of " << name() << " changed to " << val << endl; 
    }
    
    lastvalstr = txt.substr(4, txt.size()-6);
    if (dimsvc) dimsvc->updateService();
    
    return good;

  }

  return wait;
}
 

void service_nhqfloat::set(float x)
{

  cout << "set: " << x << endl;
  
  // check if the correct value is already set
  update(3, false);
  if (val==x) return;

  // build the command to be sent to the device
  stringstream out;
  out << cmd << "=" << setw(4) << setfill('0')
      << ( x / pow(10.0,vale) ) << "\r\n";

  // try to send the command up to 3 times
  for (int i=0; i<3; i++) {
    cout << "sending " << txt2hex(out.str()) << endl;
    dev->send(out.str());
    update(3, false);
    if (val==x) {
      return;
    }
  }
}

