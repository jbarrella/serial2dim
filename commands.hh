#ifndef SERIAL2DIM_COMMANDS_HH

#include "dim/dis.hxx"

#include "services.hh"

#include <string>
#include <iostream>

//-----------------------------------------------------------------------
// we need a pure virtual base class that can process()
class command : public DimCommand
{
public:
  command(std::string name,std::string fmt)
    : DimCommand(name.c_str(),fmt.c_str()) {} 
  virtual void process() {};
};


//-----------------------------------------------------------------------
// this class can do something useful: set a float
class command_nhqfloat : public command
{
public:
  command_nhqfloat(std::string name, service_nhqfloat* s)
    : command(name.c_str(),"F"), svc(s)
  {}

  virtual void process()
  {
    if ( getNext() ) {
      std::cout << "received command on " << svc->name() << std::endl;
      svc->set(getFloat());
    }
  }
  
protected:
  service_nhqfloat* svc;
  
};
  

//-----------------------------------------------------------------------
class command_raw : public command
{
public:
  command_raw(std::string name, serport* s)
    : command(name.c_str(),"C"), dev(s)
  {}

  virtual void process()
  {
    if ( getNext() ) {
      std::string cmd = getString();
      std::cout << "received command on " << getName()
		<< " - executing " << cmd << std::endl;
      dev->exec(cmd+"\r\n");
    }
  }
  
protected:
  serport* dev;
  
};
  


#endif

