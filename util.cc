
#include "util.hh"

#include <sstream>
#include <iomanip>

using namespace std;

string txt2hex(string txt)
{
  stringstream out;

  out << hex << setfill('0');
		       
  for (int i=0; i<txt.size(); i++) {
    out << setw(2) << int(txt[i]) << " ";
  }

  while (txt.find(0x0a) != string::npos) {
    txt.replace(txt.find(0x0a), 1, "\\n");
  }

  while (txt.find(0x0d) != string::npos) {
    txt.replace(txt.find(0x0d), 1, "\\r");
  }

  out << " '" << txt << "'";

  return out.str();
}
